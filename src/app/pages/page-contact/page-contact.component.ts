import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { BasePageComponent } from '@core/classes/base-page-component';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { NgZorroAntdModule } from '@shared/ng-zorro-antd/ng-zorro-antd.module';
import { NgOptimizedImage } from '@angular/common';
import { animate, style, transition, trigger } from '@angular/animations';
import { interval, Subscription, takeUntil } from 'rxjs';

interface IOffice {
    name: string;
    address: string;
    phone: string;
    imgName: string;
}

@Component({
    selector: 'owner-page-contact',
    standalone: true,
    imports: [CoreModule, SharedModule, NgZorroAntdModule, NgOptimizedImage],
    templateUrl: './page-contact.component.html',
    styleUrls: ['./page-contact.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('slideInOut', [
            transition(':enter', [
                style({ transform: 'translateX(100%)', opacity: 0 }),
                animate('1s ease-in', style({ transform: 'translateX(0%)', opacity: 1 })),
            ]),
            transition(':leave', [
                animate('1s ease-in', style({ transform: 'translateX(-100%)', opacity: 0 })),
            ]),
        ]),
    ],
})
export class PageContactComponent extends BasePageComponent {
    public email = 'OWNER_SYSTEM@mail.com';
    public phone = '8 800 200 06 00';
    public ourOffices: IOffice[] = [
        {
            name: 'Россия',
            address: '4-й Голутвинский пер., 1/8, стр. 1-2',
            phone: '8 (495) 258-31-91',
            imgName: 'russia',
        },
        {
            name: 'Аргентина',
            address: '4-й Голутвинский пер., 1/8, стр. 1-2',
            phone: '8 (495) 258-31-91',
            imgName: 'argentina',
        },
        {
            name: 'Турция',
            address: '4-й Голутвинский пер., 1/8, стр. 1-2',
            phone: '8 (495) 258-31-91',
            imgName: 'turkey',
        },
        {
            name: 'Черногория',
            address: '4-й Голутвинский пер., 1/8, стр. 1-2',
            phone: '8 (495) 258-31-91',
            imgName: 'montenegro',
        },
        {
            name: 'Комбоджиа',
            address: '4-й Голутвинский пер., 1/8, стр. 1-2',
            phone: '8 (495) 258-31-91',
            imgName: 'cambodia',
        },
        {
            name: 'Тайланд',
            address: '4-й Голутвинский пер., 1/8, стр. 1-2',
            phone: '8 (495) 258-31-91',
            imgName: 'thailand',
        },
    ];
    public activeSight = '';
    private _interval: Subscription | null = null;

    public constructor(private _changeDetector: ChangeDetectorRef) {
        super();
    }

    public mel(activeSight = ''): void {
        this._interval?.unsubscribe();
        this.activeSight = activeSight;
    }

    public start(): void {
        let index = 0;

        this._interval = interval(5000).pipe(takeUntil(this._onDestroy$)).subscribe(() => {
            this.activeSight = this.ourOffices[index].imgName;
            this._changeDetector.markForCheck();

            if (index === this.ourOffices.length - 1) {
                index = 0;
            } else {
                index++;
            }
        });
    }

    public getPhone(phone: string): string {
        return phone ? phone.replace(/\D/g, '') : '';
    }
}
